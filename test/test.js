const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('has the default page', async () => {
    const {text} = await request(app)
      .get('/');
    const { message } = JSON.parse(text);
    console.log("response is", message);
      expect(message).toEqual("Welcome to express")
  });

  it('has the default page', async () => {
    const {text} = await request(app)
      .get('/');
    const { message } = JSON.parse(text);
    console.log("response is", message);
      expect(message).toEqual("Welcome to express")
  }); 
  
  it('a failing test', async () => {
    const {text} = await request(app)
      .get('/');
    const { message } = JSON.parse(text);
    console.log("response is", message);
      expect(message).toEqual("Hello world")
  });
}); 


